export class BooksService {

  booksArray = [
    {title: 'Alice in Wonderland', author: 'Lewis Carrol'},
    {title: 'War and Peace', author: 'Leo Tolstoy'},
    {title: 'The Magic Mountain', author: 'Thomas Mann'}
  ];

  getBooks() {
    return this.booksArray;
  }

  addBooks(title: string, author: string) {
    this.booksArray.push(
      { title: title, author: author }
    );
  }
 }
