import { Post } from './../Interfaces/post.interface';
import { Observable } from 'rxjs';
import { PostsService } from './posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  posts$: Observable<Post[]>;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.posts$ = this.postsService.getAllPosts();
  }

  addToFirestore() {
    this.postsService.addToFirestore();
  }
}
