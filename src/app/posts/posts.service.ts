
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UsersService } from './users.service';
import { map, catchError } from 'rxjs/operators';
// import { AuthorsService } from './../authors/authors.service';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Post } from './../Interfaces/post.interface';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = 'https://jsonplaceholder.typicode.com/posts';
  private posts: Observable<Post[]>;
  private itemsCollection: AngularFirestoreCollection<Post>;


  constructor(private http: HttpClient,
              private usersService: UsersService,
              private db: AngularFirestore) {
                this.itemsCollection = db.collection('posts');
               }

  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.URL}`)
    .pipe(
      map((data) => this.transformPostData(data)),
      catchError(this.handleError)
    );
  }

  private handleError(res: HttpErrorResponse) {
    console.log(res.error);
    return throwError(res.error);
  }

  transformPostData(data: Post[]): Post[] {
    // const authors = this.authorsService.getAuthors();
    const users = this.usersService.getAllUsers();
    const ds: Post[] = [];

    users.forEach(users => {
      users.forEach(user => {
        data.forEach(post => {
          if (user.id === post.userId) {
            post.authorName = user.name;
            ds.push({
              userId: post.userId,
              id: post.id,
              authorName: post.authorName,
              title: post.title,
              body: post.body,
              email: user.email,
              phone: user.phone
            });
            // console.log(ds.length)
          }
        });
      });
    });

    // for (const id of users) {
    //   data.forEach(el => {
    //     if (id.id === el.userId) {
    //       el.authorName = id.name;
    //       ds.push({
    //         userId: el.userId,
    //         id: el.id,
    //         authorName: el.authorName,
    //         title: el.title,
    //         body: el.body
    //       });
    //     }
    //   });
    // }
    // console.log(ds);
    return ds;
  }

  addToFirestore() {
    const users = this.usersService.getAllUsers();
    const posts = this.http.get<Post[]>(`${this.URL}`);
    let items: Observable<Post[]>;
    items = this.itemsCollection.valueChanges();

    items.forEach(item => {
      if (item.length === 0) {
        posts.forEach(post => {
          post.forEach(postDetail => {
            users.forEach(user => {
              user.forEach(userDetail => {
                if (userDetail.id === postDetail.userId) {
                  postDetail.authorName = userDetail.name;
                  this.db.collection('posts').add({
                    title: postDetail.title,
                    body: postDetail.body,
                    author: postDetail.authorName
                  });
                }
              });
            });
          });
        });
      } else {
        console.log('All posts were persisted to your fireStore');
      }
    });
  }
}

