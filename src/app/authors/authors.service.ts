import { Observable } from 'rxjs';
export class AuthorsService {
  authorsArray = [
    {id: 1, name: 'Lewis Carrol'},
    {id: 2, name: 'Leo Tolstoy'},
    {id: 3, name: 'Thomas Mann'},
    {id: 4, name: 'Moshe ufnik'},
    {id: 5, name: 'Luke Skywalker'},
    {id: 6, name: 'Eric Bachman'},
    {id: 7, name: 'Pied Piper'},
    {id: 8, name: 'Dinesh'},
    {id: 9, name: 'Gilfoyle'},
    {id: 10, name: 'Linus Torvalds'},
  ];

  getAuthors() {
    return this.authorsArray;
  }

  editAuthor(id: number, name: string) {
    this.authorsArray.forEach(author => {
      if (author.id == id) {
        author.name = name;
      }
    });
  }

  addAuthors(name) {
    this.authorsArray.push({id: (this.authorsArray.length + 1), name: name });
  }

  getAuthorsObs(): any {
    const authorsObs = new Observable(observer => {
      observer.next(this.authorsArray);
    });
    return authorsObs;
  }

}
