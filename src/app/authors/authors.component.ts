import { AuthorsService } from './authors.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

export interface Author {
  id: number;
  name: string;
}

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})

export class AuthorsComponent implements OnInit {
  id: number;
  name: string;
  authors$: Observable<any>;
  authors: any;


  constructor(private route: ActivatedRoute, private authorService: AuthorsService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.params.id;
    this.name = this.route.snapshot.params.author;

    if (this.id !== undefined) {
      this.authorService.editAuthor(this.id, this.name);
      this.authors$ = this.authorService.getAuthorsObs();
    }
    this.authors$ = this.authorService.getAuthorsObs();
  }

}
