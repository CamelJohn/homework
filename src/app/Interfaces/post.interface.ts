export class Post {
  userId: number;
  id: number;
  title: string;
  body: string;
  authorName: string = null;
  email: string = null;
  phone: number = null;
}
