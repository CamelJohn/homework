import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.css']
})
export class EditAuthorComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute) { }
  authorEdit: string;
  authorId: number;

  ngOnInit() {
    this.authorEdit = this.route.snapshot.params['authorName'];
    this.authorId = this.route.snapshot.params['authorId'];
  }

  onSubmit() {
    this.router.navigate(['/authors', this.authorId, this.authorEdit]);
  }

}
