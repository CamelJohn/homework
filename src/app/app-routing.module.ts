import { PostsComponent } from './posts/posts.component';
import { AddAuthorComponent } from './add-author/add-author.component';
import { AddBookComponent } from './add-book/add-book.component';
import { EditAuthorComponent } from './edit-author/edit-author.component';
import { AuthorsComponent } from './authors/authors.component';
import { BooksComponent } from './books/books.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  { path: '', redirectTo: 'books', pathMatch: 'full'},
  { path: 'books', component: BooksComponent},
  { path: 'authors', component: AuthorsComponent },
  { path: 'authors/:id/:author', component: AuthorsComponent },
  { path: 'authors/:authorId/:authorName/edit', component: EditAuthorComponent },
  { path: 'addBooks', component: AddBookComponent },
  { path: 'addAuthor', component: AddAuthorComponent },
  { path: 'posts', component: PostsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
