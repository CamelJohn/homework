import { AuthorsService } from './../authors/authors.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent implements OnInit {

  author: string;
  newAuthor = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.newAuthor.hasError('required') ? 'You must enter a author name' : '';
  }

  constructor(private authorService: AuthorsService, private router: Router) { }

  ngOnInit() {
  }
  addAuthor() {
    this.authorService.addAuthors(this.author);
    this.router.navigate(['/authors']);
  }
}
