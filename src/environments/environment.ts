// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCfg9NVuB1EOTapKC1j9aCX4DrTgzJC6Y8",
    authDomain: "books-2ae09.firebaseapp.com",
    databaseURL: "https://books-2ae09.firebaseio.com",
    projectId: "books-2ae09",
    storageBucket: "books-2ae09.appspot.com",
    messagingSenderId: "575214325869",
    appId: "1:575214325869:web:e635902dfced37db8e2f64",
    measurementId: "G-N7JX99Q2PF"
  }

}


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
